CXX=g++ -std=c++11 -Wall -pedantic -Werror=vla
all: main

main: tests.cpp myvector.cpp myvector.h
	$(CXX) main.cpp myvector.cpp
tests: tests.o tests.cpp myvector.cpp myvector.h
	$(CXX) tests.o myvector.cpp
tests.o: tests.cpp
	$(CXX) -c -o tests.o tests.cpp
